/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ptg.fluentleniummavenEx.test;

import com.ptg.fluentleniummavenex.LoginPage;
import org.fluentlenium.adapter.FluentTest;
import org.fluentlenium.core.annotation.Page;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 *
 * @author vijaya laxmi
 */
public class LoginPageTest extends FluentTest{
    public WebDriver WebDriver =  new  HtmlUnitDriver();
    @ Page
    public LoginPage loginpage;
    @Test
    public  void  testLoginPage () { 
        //  Go  on  the  page  Home 
        goTo(loginpage);
        loginpage. isAt();
    }
    @Test
    public  void  validateLoginPage () { 
       loginpage.getUrl();
       loginpage.ckeckforvalidation("vijaya","vijaya12345");
       loginpage.submit();
    }
    @ Override 
    public WebDriver getDefaultDriver () { 
        return WebDriver;
     }
  
   //  Assert 
       // Assertions. assertThat (fiboResultPage. getResultat (rank)). isEqualTo (expected);
        //assertThat(title()).contains("FluentLenium");
   }
