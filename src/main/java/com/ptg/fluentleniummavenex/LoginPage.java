/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ptg.fluentleniummavenex;
import org.fest.assertions.Assertions;
import org.fluentlenium.core.FluentPage;
/**
 *
 * @author vijaya laxmi
 */
public class LoginPage extends  FluentPage{
     private final static String URL = " https://localhost:8080/fluentleniummavenex " ;
      private final static String TITLE = " LoginApp " ; 
      @ Override
      public String getUrl () { return URL;
      }
      @ Override 
      public void isAt () { 
        Assertions. assertThat ( title ()). isEqualTo (TITLE);
     } 
      public void ckeckforvalidation (String mail, String pass) { 
        fillSelect("#email").withValue(mail);
        fillSelect("#password").withValue(pass);
        $( " #submit " ). click ();
      }
      
    public  void  submit () { 
        submit ( "#submit " );
     }
}
